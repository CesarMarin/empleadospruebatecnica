Buenas días/tardes Jose

Te comparto el examen finalizado. 

Es una aplicación de 5 capas:
Presentación, acceso a datos, negocio, entidades y modelviews

Esta hecha con NET CORE 5 para que puedas correrla en tu equipo. 
Te recomiendo clonar el proyecto y ejecutarlo. En caso de que una dependencia de NPM en el cliente no funcione,
ayudame corriendo un npm install en la carpeta ClientApp para descargar los paquetenes de NODE.

La cadena de la BD esta en el archivo appSettings.json, actualmente esta con una configuración de autenticación
por usuario y contraseña, si lo deseas lo puedes cambiar a windows authentication.
La base de datos se crea cuando se corre el proyecto por primera vez. :)
