﻿using EmpleadosSistema.ModelViews;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmpleadosSistema.Repository.Contracts
{
    public interface IAreaRepository
    {
        /// <summary>
        /// Consultas las áreas activas en la BD
        /// </summary>
        /// <returns>List<AreaSelectModel></returns>
        Task<List<AreaSelectModel>> GetAreas();
    }
}
