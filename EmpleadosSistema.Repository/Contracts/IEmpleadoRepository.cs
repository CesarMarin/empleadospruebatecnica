﻿using EmpleadosSistema.ModelViews;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmpleadosSistema.Repository.Contracts
{
    public interface IEmpleadoRepository
    {
        /// <summary>
        /// Consulta los empleados activos de la BD
        /// </summary>
        /// <returns>List<EmpleadoDataTableModel></returns>
        Task<List<EmpleadoDataTableModel>> GetEmpleados();

        /// <summary>
        /// Registra un nuevo empleado
        /// </summary>
        /// <returns>True en caso de que se registre el empleado, False caso contrario</returns>
        Task<bool> NewEmpleado(EmpleadoParametersClient empleadoParametersClient);

        /// <summary>
        /// Edita un empleado
        /// </summary>
        /// <returns>True en caso de que se edite el empleado, False caso contrario</returns>
        Task<bool> PutEmpleado(int Id, EmpleadoParametersClient empleadoParametersClient);

        /// <summary>
        /// Desactiva/Activa un empleado
        /// </summary>
        /// <returns>True en caso de que cambie el estatus del empleado, False caso contrario</returns>
        Task<bool> DeleteEmpleado(int Id);
    }
}
