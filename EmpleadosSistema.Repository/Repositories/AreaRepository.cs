﻿using EmpleadosSistema.DataAccess;
using EmpleadosSistema.ModelViews;
using EmpleadosSistema.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmpleadosSistema.Repository.Repositories
{
    public class AreaRepository : IAreaRepository
    {
        private readonly EmpleadosContext _empleadosContext;
        public AreaRepository(EmpleadosContext empleadosContext)
        {
            _empleadosContext = empleadosContext;
        }

        public async Task<List<AreaSelectModel>> GetAreas()
        {
            try
            {
                return await _empleadosContext.Areas
                    .Where(x => x.Status)
                    .Select(x => new AreaSelectModel()
                    {
                        Id = x.Id,
                        Descripcion = x.Descripcion
                    })
                    .ToListAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
