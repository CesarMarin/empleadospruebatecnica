﻿using EmpleadosSistema.DataAccess;
using EmpleadosSistema.Entities;
using EmpleadosSistema.ModelViews;
using EmpleadosSistema.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmpleadosSistema.Repository.Repositories
{
    public class EmpleadoRepository : IEmpleadoRepository
    {
        private readonly EmpleadosContext _empleadosContext;
        public EmpleadoRepository(EmpleadosContext empleadosContext)
        {
            _empleadosContext = empleadosContext;
        }

        public async Task<List<EmpleadoDataTableModel>> GetEmpleados()
        {
            try
            {
                return await _empleadosContext.Empleados
                    .Select(x => new EmpleadoDataTableModel()
                    {
                        Id = x.Id,
                        IdArea = x.IdArea,
                        Nombre = x.Nombre,
                        Telefono = x.Telefono,
                        Area = x.Area.Descripcion,
                        Status = x.Status
                    })
                    .OrderBy(x => x.Nombre)
                    .ToListAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> NewEmpleado(EmpleadoParametersClient empleadoParametersClient)
        {
            try
            {
                await _empleadosContext.Empleados.AddAsync(new()
                {
                    IdArea = empleadoParametersClient.IdArea,
                    Nombre = empleadoParametersClient.Nombre,
                    Telefono = empleadoParametersClient.Telefono
                });

                return await _empleadosContext.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> PutEmpleado(int Id, EmpleadoParametersClient empleadoParametersClient)
        {
            try
            {
                Empleado empleado = await _empleadosContext.Empleados.Where(x => x.Status && x.Id == Id).FirstOrDefaultAsync();
                if (empleado == null)
                {
                    return false;
                }

                empleado.Nombre = empleadoParametersClient.Nombre;
                empleado.IdArea = empleadoParametersClient.IdArea;
                empleado.Telefono = empleadoParametersClient.Telefono;
                return await _empleadosContext.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> DeleteEmpleado(int Id)
        {
            try
            {
                Empleado empleado = await _empleadosContext.Empleados.Where(x => x.Id == Id).FirstOrDefaultAsync();
                if (empleado == null)
                {
                    return false;
                }

                empleado.Status = !empleado.Status;
                return await _empleadosContext.SaveChangesAsync() > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
