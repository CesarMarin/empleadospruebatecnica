﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace EmpleadosSistema.DataAccess
{
    /// <summary>
    /// Semilla que crea y pobla la BD (cuando no existe).
    /// </summary>
    public class Seeder
    {
        /// <summary>
        /// Crea la BD y pobla el catálogo de áreas cuando no existe la BD.
        /// </summary>
        public static void Init(EmpleadosContext _empleadosContext)
        {
            if (!(_empleadosContext.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists())
            {
                _ = _empleadosContext.Database.EnsureCreated();

                _ = _empleadosContext.Areas.Add(new() { Descripcion = "Recursos humanos" });
                _ = _empleadosContext.Areas.Add(new() { Descripcion = "Comercial y ventas" });
                _ = _empleadosContext.Areas.Add(new() { Descripcion = "Tecnológica" });
                _ = _empleadosContext.Areas.Add(new() { Descripcion = "Soporte Técnico" });

                _ = _empleadosContext.SaveChanges();
            }
        }

    }
}
