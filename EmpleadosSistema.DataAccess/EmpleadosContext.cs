﻿using EmpleadosSistema.Entities;
using Microsoft.EntityFrameworkCore;

namespace EmpleadosSistema.DataAccess
{
    /// <summary>
    /// Contexto principal de la aplicación
    /// </summary>
    public partial class EmpleadosContext : DbContext
    {

        public EmpleadosContext(DbContextOptions<EmpleadosContext> options) : base(options) { }

        public virtual DbSet<Empleado> Empleados { get; set; }

        public virtual DbSet<Area> Areas { get; set; }


        /// <summary>
        /// Parametrización de las entidades de la BD
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            _ = modelBuilder.Entity<Empleado>(entity =>
            {
                _ = entity.ToTable("empleados");

                _ = entity.Property(x => x.Id).ValueGeneratedOnAdd();

                _ = entity.HasIndex(x => x.IdArea, "IdArea");
                _ = entity.HasOne(x => x.Area).WithMany(x => x.Empleados).HasForeignKey(x => x.IdArea).OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("empleado_area_fk");

                _ = entity.Property(x => x.Nombre).HasMaxLength(200);

                _ = entity.HasIndex(x => x.Telefono, "Telefono_UNIQUE").IsUnique();
                _ = entity.Property(x => x.Telefono).HasMaxLength(10);

                _ = entity.Property(x => x.DateCreated).HasColumnType("datetime");

            });

            _ = modelBuilder.Entity<Area>(entity =>
            {
                _ = entity.ToTable("areas");

                _ = entity.Property(x => x.Id).ValueGeneratedOnAdd();

                _ = entity.Property(x => x.Descripcion).HasMaxLength(200);
                _ = entity.HasIndex(x => x.Descripcion, "Descripcion_UNIQUE").IsUnique();

                _ = entity.Property(x => x.DateCreated).HasColumnType("datetime");

            });
        }

    }
}
