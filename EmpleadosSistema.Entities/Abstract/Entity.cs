﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmpleadosSistema.Entities.Abstract
{
    /// <summary>
    /// Módelo de datos para todas las entidades (Tablas) de la BD
    /// </summary>
    public abstract class Entity
    {
        public Entity()
        {
            DateCreated = DateTime.Now;
            Status = true;
        }

        [Required]
        public int Id { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        [Required]
        public bool Status { get; set; }

    }
}
