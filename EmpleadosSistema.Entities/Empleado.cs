﻿using EmpleadosSistema.Entities.Abstract;
using System.ComponentModel.DataAnnotations;

namespace EmpleadosSistema.Entities
{
    /// <summary>
    /// Entidad de la tabla empleados
    /// </summary>
    public class Empleado : Entity
    {
        public int IdArea { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Telefono { get; set; }
        public virtual Area Area { get; set; }
    }
}
