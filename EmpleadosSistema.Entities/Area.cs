﻿using EmpleadosSistema.Entities.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace EmpleadosSistema.Entities
{
    /// <summary>
    /// Entidad de la tabla áreas
    /// </summary>
    public class Area : Entity
    {
        [Required]
        public string Descripcion { get; set; }
        public virtual ICollection<Empleado> Empleados { get; set; }
    }
}
