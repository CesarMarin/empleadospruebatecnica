﻿using EmpleadosSistema.ModelViews;
using EmpleadosSistema.Repository.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmpleadosSistema.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AreasController : ControllerBase
    {
        private readonly IAreaRepository _areaRepository;

        public AreasController(IAreaRepository areaRepository)
        {
            _areaRepository = areaRepository;
        }

        /// <summary>
        /// Endpoint para obtener la lista de áreas
        /// </summary>
        /// <returns></returns>
        // GET: api/Areas
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<AreaSelectModel>))]
        public async Task<ActionResult<IEnumerable<AreaSelectModel>>> GetAreas()
        {
            try
            {
                return await _areaRepository.GetAreas();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
