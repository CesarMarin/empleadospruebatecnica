﻿using EmpleadosSistema.Entities;
using EmpleadosSistema.ModelViews;
using EmpleadosSistema.Repository.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmpleadosSistema.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadosController : ControllerBase
    {
        private readonly IEmpleadoRepository _empleadoRepository;

        public EmpleadosController(IEmpleadoRepository empleadoRepository)
        {
            _empleadoRepository = empleadoRepository;
        }

        /// <summary>
        /// Endpoint para obtener la lista de empleados
        /// </summary>
        /// <returns>Lista de empleados</returns>
        // GET: api/Empleados
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<EmpleadoDataTableModel>))]
        public async Task<ActionResult<IEnumerable<EmpleadoDataTableModel>>> GetEmpleados()
        {
            try
            {
                return await _empleadoRepository.GetEmpleados();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Endpoint para registrar un nuevo empleado
        /// </summary>
        /// <param name="empleadoParametersClient"></param>
        /// <returns>Ok</returns>
        // POST: api/Empleados
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Empleado>> PostEmpleado(EmpleadoParametersClient empleadoParametersClient)
        {
            try
            {
                return await _empleadoRepository.NewEmpleado(empleadoParametersClient) ? Ok() : BadRequest();
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Endpoint para editar un empleado
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="empleadoParametersClient"></param>
        /// <returns>Ok</returns>
        // POST: api/Empleados
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{Id}")]
        public async Task<ActionResult<Empleado>> PutEmpleado(int Id, EmpleadoParametersClient empleadoParametersClient)
        {
            try
            {
                return await _empleadoRepository.PutEmpleado(Id, empleadoParametersClient) ? Ok() : BadRequest();
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Endpoint para desactivar/activar un empleado
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Ok</returns>
        // Delete: api/Empleados/{id}
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpDelete("{Id}")]
        public async Task<ActionResult> DeleteEmpleado(int Id)
        {
            try
            {
                return await _empleadoRepository.DeleteEmpleado(Id) ? Ok() : BadRequest();
            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}
