import { Container } from 'reactstrap';
import { NavMenu } from '../shared/NavMenu';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


// Layout o plantilla de la aplicación
const MainLayout = ({ children }) => {
    return (
        <div>
            <NavMenu />
            <Container>
                {children}
            </Container>
            <ToastContainer />
        </div>
    );
}

export default MainLayout;