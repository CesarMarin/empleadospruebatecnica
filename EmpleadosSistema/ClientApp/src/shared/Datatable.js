﻿import React from 'react';
import DataTable, { defaultThemes } from 'react-data-table-component';
import DataTableExtensions from "react-data-table-component-extensions";
import "react-data-table-component-extensions/dist/index.css";

//NOTE Estilos para la tabla
const customStyles = {
    headRow: {
        style: {
            borderTopStyle: 'solid',
            borderTopWidth: '1px',
            borderTopColor: defaultThemes.default.divider.default
        }
    },
    headCells: {
        style: {
            letterSpacing: "1px",
            textTransform: "uppercase",
            backgroundColor: "#f6f9fc",
            color: "#8898aa",
            weight: "bold"
        }
    }
};

//NOTE Componente principal que retorna el dataTable
const DataTableCustom = ({ columns, data }) => {

    return (
        <DataTableExtensions columns={columns} data={data} filterPlaceholder="Busque un texto" export={false} print={false}>
            <DataTable
                noDataComponent="No se encontro información"
                defaultSortAsc={false}
                customStyles={customStyles}
                noHeader
                highlightOnHover
                pagination
                paginationComponentOptions={{
                    rowsPerPageText: 'Registros por página:',
                    rangeSeparatorText: 'de',
                    noRowsPerPage: false,
                    selectAllRowsItem: true,
                    selectAllRowsItemText: 'Todos'
                }}
            />
        </DataTableExtensions>
    );
};

export default DataTableCustom;