import { useEffect, useState } from 'react';
import axios from 'axios';
import SweetAlert from 'react-bootstrap-sweetalert';
import { toast } from 'react-toastify';
import { Form, FormGroup, Label, Input, Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import DataTableCustom from '../shared/Datatable';

// Componente para el CRUD de empleados
const DataEmployees = () => {

    const [empleados, setEmpleados] = useState([]);
    const [areas, setAreas] = useState([]);

    const [showSweet, setShowSweet] = useState(false);
    const [titleStatus, setTitleStatus] = useState("");

    const [showModal, setShowModal] = useState(false);
    const [typeModal, setTypeModal] = useState(0);
    const [idSelected, setIdSelected] = useState("");

    const formEmpleado = {
        nombre: '',
        telefono: '',
        idArea: ''
    };
    const [empleado, setEmpleado] = useState(formEmpleado);

    useEffect(() => {
        getEmpleados();
        getAreas();
        return (() => { });
    }, []);

    const getAreas = async () => {
        try {
            let { data, status } = await axios.get("/api/Areas");
            status === 200 && setAreas(data);
        } catch (e) {
            toast.error("Ocurrio un error al cargar la lista de áreas.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                autoClose: 2500
            });
        }
    }

    const getEmpleados = async () => {
        try {
            let { data, status } = await axios.get("/api/Empleados");
            status === 200 && setEmpleados(data);
        } catch (e) {
            toast.error("Ocurrio un error al cargar la lista de empleados.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                autoClose: 2500
            });
        }
    }

    const newEmpleado = async (e) => {
        e.preventDefault();
        try {
            const { nombre, telefono, idArea } = empleado;
            let { status } = await axios.post("/api/Empleados", {
                nombre,
                telefono,
                idArea
            });

            if (status === 200) {
                getEmpleados();
                toast.success("Registro exitoso", {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    autoClose: 2500
                });
            }
        } catch (e) {
            toast.error("Ocurrio un error al registrar el empleado, verifique que los datos no esten duplicados o sean incorrectos.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                autoClose: 2500
            });
        }
        setEmpleado(formEmpleado);
        setIdSelected("");
        setShowModal(false);
    }

    const updateEmpleado = async (e) => {
        e.preventDefault();
        try {
            const { nombre, telefono, idArea } = empleado;
            let { status } = await axios.put(`/api/Empleados/${idSelected}`, {
                nombre,
                telefono,
                idArea
            });
            if (status === 200) {
                getEmpleados();
                toast.success("Registro exitoso.", {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    autoClose: 2500
                });
            }
        } catch (e) {
            toast.error("Ocurrio un error al editar el empleado, verifique que los datos no esten duplicados o sean incorrectos.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                autoClose: 2500
            });
        }
        setEmpleado(formEmpleado);
        setIdSelected("");
        setShowModal(false);
    }

    const changeStatus = async () => {
        try {
            let { status } = await axios.delete(`/api/Empleados/${idSelected}`);
            if (status === 200) {
                getEmpleados();
                toast.success("Cambio de estatus exitoso.", {
                    position: toast.POSITION.BOTTOM_RIGHT,
                    autoClose: 2500
                });
            }
        } catch (e) {
            toast.error("Ocurrio un error al cambiar el estatus del empleado.", {
                position: toast.POSITION.BOTTOM_RIGHT,
                autoClose: 2500
            });
        }
        setShowSweet(false);
        setIdSelected("");
    }

    const columns = [
        {
            name: 'Nombre del empleado',
            selector: ({ nombre }) => nombre,
            sortable: true,
        },
        {
            name: 'Teléfono',
            selector: ({ telefono }) => telefono,
            sortable: true,
        },
        {
            name: 'Área',
            selector: ({ area }) => area,
            sortable: true,
        },
        {
            name: 'Acciones',
            cell: ({ id, idArea, nombre, telefono, status }) =>
                <>
                    <button className="btn btn-outline-info mr-2"
                        disabled={!status}
                        onClick={() => {
                            setTypeModal(2);
                            setIdSelected(id);
                            setShowModal(true);
                            setEmpleado({
                                ...empleado,
                                idArea,
                                nombre,
                                telefono
                            });
                        }}>
                        Editar
                    </button>
                    <button className={`btn ${status ? "btn-outline-danger" : "btn-outline-success"}`}
                        onClick={() => {
                            setIdSelected(id);
                            setShowSweet(true);
                            setTitleStatus(`¿Esta seguro de ${status ? "desactivar" : "activar"} el registro?`);
                        }}>
                        {
                            status ?
                                <>Desactivar</>
                                :
                                <>Activar</>
                        }
                    </button>
                </>
        }
    ];

    return (
        <>
            <h3>Sistema de registro de empleados</h3>
            <p style={{ fontWeight: 'bold' }}>Instrucciones:</p>
            <p style={{ marginBottom: 0 }}>1.- Para registrar un nuevo empleado, presione el botón "Nuevo empleado".</p>
            <p style={{ marginBottom: 0 }}>2.- Para editar la información de un empleado, presione el botón "Editar" de la fila correspondiente al empleado.</p>
            <p>3.- Para desactivar/activar un empleado, presione el botón "Desactivar" y/o "Activar" de la fila correspondiente al empleado.</p>

            <div className="row" align="right">
                <div className="col-4 offset-md-8">
                    <Button className="btn btn-success" onClick={() => {
                        setTypeModal(1);
                        setShowModal(true);
                    }}>Nuevo empleado</Button>
                </div>
            </div>

            <DataTableCustom
                columns={columns}
                data={empleados}
            />

            <Modal isOpen={showModal} toggle={() => {
                setShowModal(false);
                setEmpleado(formEmpleado);
            }}>
                <ModalHeader toggle={() => {
                    setShowModal(false);
                    setEmpleado(formEmpleado);
                }}>{typeModal === 1 ? "Nuevo empleado" : "Editar empleado"}</ModalHeader>
                <Form onSubmit={ typeModal === 1 ? newEmpleado : updateEmpleado }>
                    <ModalBody>
                        <FormGroup>
                            <Label for="nombre">
                                {`Nombre del empleado`}
                            </Label>
                            <Input
                                id="nombre"
                                placeholder="Ingrese el nombre del empleado"
                                type="text"
                                required
                                maxLength="200"
                                value={empleado.nombre}
                                onChange={(e) => setEmpleado({ ...empleado, nombre: e.target.value })}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="telefono">
                                {`Teléfono del empleado`}
                            </Label>
                            <Input
                                id="telefono"
                                placeholder={`Ingrese el teléfono del empleado`}
                                type="text"
                                required
                                maxLength="10"
                                value={empleado.telefono}
                                onChange={(e) => {
                                    const newValue = e.target.value;
                                    !isNaN(newValue) && setEmpleado({ ...empleado, telefono: newValue })
                                }}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="area">
                                {`Seleccione el área en la que trabaja el empleado`}
                            </Label>
                            <Input
                                id="area"
                                type="select"
                                required
                                value={empleado.idArea}
                                onChange={(e) => setEmpleado({ ...empleado, idArea: e.target.value })}
                            >
                                <option value="">Seleccione una opcion</option>
                                {
                                    areas.length > 0 &&
                                    areas.map(({ id, descripcion }) => {
                                        return (
                                            <option key={id} value={id}>{descripcion}</option>
                                        );
                                    })
                                }
                            </Input>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="danger" onClick={() => {
                            setEmpleado(formEmpleado);
                            setShowModal(false);
                        }}>
                            Cancelar
                        </Button>
                        {' '}
                        <Button color="primary">
                            Confirmar
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>

            <SweetAlert show={showSweet} type="info" title={titleStatus}
                showCancel cancelBtnText="Cancelar" cancelBtnBsStyle="danger" onCancel={() => setShowSweet(false)}
                confirmBtnText="Aceptar" confirmBtnBsStyle="info" onConfirm={changeStatus}
            />
        </>
    );
}

export default DataEmployees;
