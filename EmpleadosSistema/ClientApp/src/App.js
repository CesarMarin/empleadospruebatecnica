import { Route } from 'react-router';
import MainLayout from './layouts/Layout';
import DataEmployees from './components/DataEmployees';

// Componente principal
const App = () => {
    return (
        <MainLayout>
            <Route exact path='/' component={DataEmployees} />
        </MainLayout>
    );
}

export default App;