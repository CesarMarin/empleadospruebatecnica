﻿namespace EmpleadosSistema.ModelViews
{
    public class EmpleadoDataTableModel
    {
        public int Id { get; set; }
        public int IdArea { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public string Area { get; set; }
        public bool Status { get; set; }
    }

    public class EmpleadoParametersClient
    {
        public int IdArea { get; set; }
        public string Nombre { get; set; }
        public string Telefono { get; set; }
    }
}
