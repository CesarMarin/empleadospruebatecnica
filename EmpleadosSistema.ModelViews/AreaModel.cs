﻿namespace EmpleadosSistema.ModelViews
{
    /// <summary>
    /// Modelo para el select de la vista
    /// </summary>
    public class AreaSelectModel
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
    }
}
